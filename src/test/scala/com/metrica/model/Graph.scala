package com.metrica.model

import scala.util.parsing.input.CharSequenceReader
import org.specs2.mutable._

private[model] object Dummy extends ExpressionParsing {

  def parsing[T](s: String)(implicit p: Parser[T]): T =
    phrase(p)(new CharSequenceReader(s)) match {
      case Success(t, _)     => t
      case NoSuccess(msg, _) => throw new IllegalArgumentException(
        "Could not parse '" + s + "': " + msg)
   }
}

class ExpressionParserSpecification extends Specification {

  import Dummy._

  "ExpressionParsing" should { 

    "parse boolean literals" in {
      implicit val parserToTest = boolean
      parsing("true") must_== BooleanLiteral(true)
      parsing("false") must_== BooleanLiteral(false)
      parsing("FALSE") must throwA[IllegalArgumentException]
      parsing("truefoo") must throwA[IllegalArgumentException]
      parsing("TRUE") must throwA[IllegalArgumentException]
      parsing("True") must throwA[IllegalArgumentException]
      parsing("False") must throwA[IllegalArgumentException]
    }

    "parse floating point numbers" in {
      implicit val parserToTest = double
      parsing("0.0")     must_== (NumberLiteral(0.0))
      parsing("1.0")     must_== (NumberLiteral(1.0))
      parsing("-1.0")    must_== (NumberLiteral(-1.0))
      parsing("0.2")     must_== (NumberLiteral(0.2))
      parsing("-0.2")    must_== (NumberLiteral(-0.2))
      parsing(".2")      must_== (NumberLiteral(.2))
      parsing("-.2")     must_== (NumberLiteral(-.2))
      parsing("2.0e3")   must_== (NumberLiteral(2000.0))
      parsing("2.0E3")   must_== (NumberLiteral(2000.0))
      parsing("-2.0e3")  must_== (NumberLiteral(-2000.0))
      parsing("-2.0E3")  must_== (NumberLiteral(-2000.0))
      parsing("2.0e-3")  must_== (NumberLiteral(0.002))
      parsing("2.0E-3")  must_== (NumberLiteral(0.002))
      parsing("-2.0e-3") must_== (NumberLiteral(-0.002))
      parsing("-2.0E-3") must_== (NumberLiteral(-0.002))
    }

    "parse integral numbers" in {
      implicit val parserToTest = int
      parsing("0")    must_== (NumberLiteral(0))
      parsing("1")    must_== (NumberLiteral(1))
      parsing("-1")   must_== (NumberLiteral(-1))
      parsing("20")   must_== (NumberLiteral(20))
      parsing("-20")  must_== (NumberLiteral(-20))
    }

    "parse string literals" in {
      implicit val parserToTest = string
      parsing("\"test\"") must_== StringLiteral("\"test\"")
      parsing("\"\"") must_== StringLiteral("\"\"")
      parsing("\"test") must throwA[IllegalArgumentException]
      parsing("test") must throwA[IllegalArgumentException]
      parsing("\"te\"st\"") must throwA[IllegalArgumentException]
      //TODO: add interesting cases once we have proper escape handling
    }

    "parse metric names" in {
      implicit val parserToTest = metric
      parsing("foo.bar.baz") must_== (Metric("foo.bar.baz"))
      parsing("_foo.bar") must_== (Metric("_foo.bar"))
      parsing("foo_bar.baz") must_== (Metric("foo_bar.baz"))

      parsing("foo-bar") must throwA[IllegalArgumentException]
      parsing("+foo") must throwA[IllegalArgumentException]
      parsing("foo+") must throwA[IllegalArgumentException]
      parsing("foo") must throwA[IllegalArgumentException]
      parsing("") must throwA[IllegalArgumentException]
    }

    "parse function calls" in {
      implicit val parserToTest = fcall
      parsing("alias(foo.baz, \"bar\")") must_==
        Call("alias", List(Metric("foo.baz"), StringLiteral("\"bar\"")))
      parsing("alias(foo.baz)") must_==
        Call("alias", List(Metric("foo.baz")))
    }

    "parse nested function calls" in {
      implicit val parserToTest = fcall
      val nested = Call("alias", List(Metric("foo.baz"), StringLiteral("\"bar\"")))
      val nestedStr =  "alias(foo.baz, \"bar\")"
      parsing("alias(" + nestedStr + ", \"baf\")") must_==
        Call("alias", List(nested, StringLiteral("\"baf\"")))
      parsing("alias(" + nestedStr + ", " + nestedStr + ")") must_==
        Call("alias", List(nested, nested))
    }

  }

  "Evaluation" should {
    val series = Series("foo.baz", List(2.2, 1.1))

    "call functions" in {
      Evaluation.call("alias", (series.title, series.items), "bar") must_== Tuple2("bar",  series.items)
      Evaluation(
        Map("foo.baz" -> series.items),
        Call("alias", List(Metric("foo.baz"), StringLiteral("\"bar\"")))
      ) must_== ("bar",  series.items)
    }

    "call nested functions" in {
      Evaluation(
        Map("foo.baz" -> series.items),
        Call("alias",
          List(
            Call("alias", List(Metric("foo.baz"), StringLiteral("\"bar\""))),
            StringLiteral("\"another\"")
          )
        )
      ) must_== ("another",  series.items)
    }

    "throw corresponding exception when series missing" in {
      Evaluation(
        Map("foo.baz" -> series.items),
        Call("alias", List(Metric("missing"), StringLiteral("\"bar\"")))
      ) must throwA[NoSuchSerie]
    }

    "throw corresponding exception when function missing" in {
      Evaluation(
        Map("foo.baz" -> series.items),
        Call("missing function", List(Metric("foo.baz"), StringLiteral("\"bar\"")))
      ) must throwA[NoSuchFunction]
    }
  }

  "Graph Expression" should {
    import Graph.Expression

    "work with plain strings" in {
      val ge = new Expression("alias(foo.bar,\"something\")")
      ge.apply(Map("foo.bar" -> List(1, 2, 3))) must_==
        ("something", List(1, 2, 3))
    }
  }

}
