package com.metrica.util

import org.specs2._
import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.config.ConfigException.Missing
import net.ceedubs.ficus.Ficus._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import org.slf4j.{ Logger, LoggerFactory }

class ConfigSpec extends Specification {
  def is = s2"""

  Util::Config specification
      SysConfig object should work $testLoad
      Should fail if default namespace missing something $missingDefault
      Should work if non-default missing something $missingNonDefault
      Should work if some files missing in non-default $missingFilesNonDefault

  """
  val l = LoggerFactory.getLogger("development")

  def testLoad = {
    val to_test = "value to test against"
    val string = new SysConfig("development", List("test"),
      loadF = (_) => ConfigFactory.parseMap(Map("com.etismy.foo" -> to_test))).load().getString("test.foo")
    string must_== to_test
  }

  def missingDefault = {
    def load(s: String): Config = {
      s match {
        case "development/test" => ConfigFactory.parseMap(
          Map("com.etismy" -> Map("foo" -> "bar").asJava).asJava
        )
        case _ => throw new Missing("default")
      }
    }
    def tryLoadAConfig = {
      new SysConfig("development", List("test"), loadF = load).load("com.etismy")
    }
    tryLoadAConfig must throwA[DefaultNamespaceMissingException]
  }

  def missingNonDefault = {
    val to_test = "value to test against"
    def load(s: String): Config = {
      s match {
        case "development/test" => ConfigFactory.empty
        case _ => ConfigFactory.parseMap(
          Map("com.etismy.foo" -> to_test)
        )
         }
    }
    new SysConfig("development", List("test"), loadF = load)
      .load().getString("test.foo") must_== to_test
  }

  def missingFilesNonDefault = {
    val to_test = "value to test against"
    def load(s: String): Config = {
      s match {
        case "development/test" => throw new Missing("development")
        case _ => ConfigFactory.parseMap(
          Map("com.etismy.foo" -> to_test)
        )
      }
    }
    new SysConfig("development", List("test"), loadF = load)
      .load().getString("test.foo") must_== to_test
  }
}
