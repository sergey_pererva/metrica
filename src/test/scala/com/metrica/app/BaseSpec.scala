package com.metrica.app

import org.scalatra.test.specs2._

class RootSpec extends ScalatraSpec {
  def is = s2"""
    GET / on Root
      should return status 200 $root200

  """

  addServlet(classOf[Root], "/*")

  def root200 = get("/") {
    status must_== 200
  }
}
