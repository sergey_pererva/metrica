package com.metrica.servlet

import org.scalatra.test.specs2._
import org.specs2.specification._
import org.scalatra.ScalatraServlet

private[servlet] class DummyServlet
    extends ScalatraServlet with WebJarsSupport

class WebJarsSupportSpec extends ScalatraSpec {
  def is = s2"""
    GET some existing asset
      should return status 200 $existing200
      should return correct content $correctBody
    GET some not existing asset
      should return 404 $nonExisting404
    """

  addServlet(classOf[DummyServlet], "/*")

  def existing200 = get("/lib/some_existing_asset.css") {
    status must_== 200
  }

  def correctBody = get("/lib/some_existing_asset.min.css") {
    body must_== "minified contents\n"
  }

  def nonExisting404 = get("/lib/do_not_exist.css") {
    status must_== 404
  }
}
