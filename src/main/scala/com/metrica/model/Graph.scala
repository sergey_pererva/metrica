package com.metrica.model

import scala.util.parsing.combinator.JavaTokenParsers
import scala.util.parsing.input.CharSequenceReader

object Evaluation {

  object Functions {

    def alias(series: List[Double], alias: String): (String, List[Double]) = (alias, series)

  }

  def call(methodName: String, args: AnyRef*): AnyRef =
    /*Functions.getClass.getMethod(methodName, args.map(_.getClass): _*)
      .invoke(Functions, args: _*)*/
    methodName match {
      case "alias" => Functions.alias(
        args(0).asInstanceOf[(String, List[Double])]._2,
        args(1).asInstanceOf[String]
      )
      case _ => throw new NoSuchFunction(methodName, args: _*)
    }

  def apply(env: Graph.Series, e: Expression): AnyRef = e match {
    case nl: NumberLiteral => nl.doubleValue.asInstanceOf[AnyRef]
    case bl: BooleanLiteral => bl.boolValue.asInstanceOf[AnyRef]
    case sl: StringLiteral => sl.stringValue
    case m: Metric => apply(env, m.eval(env))
    case s: Series => (s.title, s.items)
    case c: Call => apply(env, c.eval(env))
    case _ => "none"
  }

}

sealed trait Expression {
  def eval(env: Graph.Series): Literal
}

sealed trait Literal extends Expression {
    def eval(env: Graph.Series) = this
    def doubleValue: Double
    def boolValue: Boolean
    def stringValue: String
}

case class NumberLiteral(literal: Double) extends Literal {
    def doubleValue = literal
    def boolValue   = literal != 0.0
    def stringValue = literal.toString
    override def toString  = literal.toString
}

case class BooleanLiteral(literal: Boolean) extends Literal {
    def doubleValue = if (literal) 1.0 else 0.0
    def boolValue   = literal
    def stringValue = literal.toString
    override def toString  = literal.toString
}

case class StringLiteral(s: String) extends Literal {
    val literal = s.substring(1,s.length-1)//TODO: apply missing escapes
    def doubleValue = literal.toDouble
    def boolValue   = literal.toLowerCase != "false"
    def stringValue = literal
    override def toString  = s
}

case class Metric(name: String) extends Expression {
  def eval(env: Graph.Series) = env get name match {
    case Some(items: List[Double]) => Series(name, items)
    case _ => throw new NoSuchSerie(name)
  }
    override def toString = name
}

case class Series(val title: String, val items: List[Double])
    extends Literal {
  def stringValue = "series" + (items.reduceLeft(_ + _))
  def doubleValue = 0.0
  def boolValue = items.size != 0
  override def toString = stringValue
}

case class Call(ident: String, params: List[Expression])
    extends Expression {

  override def eval(env: Graph.Series): Literal = {
    val evaluated = Evaluation.call(ident, params.map(
      Evaluation(env, _)): _*).asInstanceOf[Tuple2[String, List[Double]]]
    Series(evaluated._1, evaluated._2)
  }

}

trait ExpressionParsing extends JavaTokenParsers {

  protected val javaIdent: String =
    """\p{javaJavaIdentifierStart}\p{javaJavaIdentifierPart}*"""

  def boolean: Parser[Expression] =
    ("true" | "false")  ^^ { s => new BooleanLiteral(s.toBoolean) }

  def string: Parser[Expression] =
    super.stringLiteral ^^ { new StringLiteral(_) }

  def double: Parser[Expression] =
    floatingPointNumber ^^ { s => new NumberLiteral(s.toDouble) }

  def int: Parser[Expression] =
    wholeNumber ^^ { s => new NumberLiteral(s.toInt) }

  def literal: Parser[Expression] = boolean | string | double | int

  def metric: Parser[Expression] =
    (javaIdent + "(\\." + javaIdent + ")+").r ^^ { s => new Metric(s) }

  def expression: Parser[Expression] = literal | metric | fcall

  def fcall: Parser[Expression] =
    ident ~ ("(" ~> repsep(expression, ",") <~ ")") ^^
      { case ident ~ params =>  new Call(ident, params) }

  def parse(s: String): Expression =
    parseAll(fcall, new CharSequenceReader(s)) match {
      case Success(result, _) => result
      case NoSuccess(msg, _) => throw new IllegalArgumentException("Could not parse '" + s + "': " + msg)
    }

}

abstract class ExpressionException extends Exception

class NoSuchSerie(serieName: String) extends ExpressionException {
  override def toString = s"There is on such serie as $serieName"
}

class NoSuchFunction(functionName: String, params: AnyRef*)
    extends ExpressionException {
  override def toString = {
    val paramsRep = params map { o: AnyRef => {
      o.toString() + ": " + o.getClass().getName()}} mkString ", "
    s"There is on such function as `$functionName($paramsRep)`"
  }
}

object Graph {

  type Serie = (String, List[Double])
  type Series = Map[String, List[Double]]

  class Expression(val contents: String) extends ExpressionParsing {

    def apply(env: Series): Serie =
      Evaluation(env, parse(contents)).asInstanceOf[Serie]

  }
}

case class Graph (
  val title: String,
  val expressions: List[Graph.Expression]
) {
  def series(env: Graph.Series) = expressions map { _.apply(env) }
}
