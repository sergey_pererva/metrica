package com.metrica.util

import scala.util.control.Exception._

object Control {

  def defining[A, B](value: A)(f: A => B): B = f(value)

  def using[A <% { def close(): Unit }, B](resource: A)(f: A => B): B =
    try f(resource) finally { resource.close() }

  def optionally[A](foption: A, default: A): A =
    defining(Option(foption)) { _ getOrElse default }
}
