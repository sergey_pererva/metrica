package com.metrica.util

import javax.servlet._
import org.slf4j.LoggerFactory
import javax.servlet.http.HttpServletRequest
import com.mchange.v2.c3p0.ComboPooledDataSource

object DB {

  private val dbConf = Sys.config.getConfig("database")
  private val cpds = new ComboPooledDataSource

  cpds.setDriverClass(dbConf.getString("driverClass"))
  cpds.setJdbcUrl(dbConf.getString("jdbcUrl"))
  cpds.setUser(dbConf.getString("user"))
  cpds.setPassword(dbConf.getString("password"))

  def apply: slick.jdbc.JdbcBackend.Database = {
    lazy val db = slick.jdbc.JdbcBackend.Database.forDataSource(cpds)
    db
  }

  def close: Unit = {
    cpds.close
  }
}
