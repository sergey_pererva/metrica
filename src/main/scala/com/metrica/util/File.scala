package com.metrica.util

import java.io.{ File, PrintWriter }
import Control.using

object Files {

  def printToFile(f: File)(op: PrintWriter => Unit): Unit = {
    maybeCreateParent(f)
    using(new PrintWriter(f))(op)
  }

  def maybeCreateParent(f: File): Unit = {
    f.getParentFile() match {
      case f: File => if(!f.exists()) f.mkdirs()
      case _ => throw new Exception("no parent dir")
    }
  }

  def deleteDirs(dirs: Array[File]): Unit = dirs map { (f: File) =>
    if(f.isDirectory()) deleteDirs(f.listFiles())
    f.delete()
  }
}
