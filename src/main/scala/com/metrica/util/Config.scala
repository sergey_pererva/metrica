package com.metrica.util

import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.config.ConfigException.Missing
import net.ceedubs.ficus.Ficus._
import com.metrica.util.Control._
import scala.util.control.Exception._
import org.slf4j.{ Logger, LoggerFactory }

private[util] class SysConfig(
  env: String,
  configTypes: List[String] = List("app", "test"),
  defaultEnv: String = "default",
  loadF: String => Config = ConfigFactory.load(_)
) {

  val l = LoggerFactory.getLogger("development")

  def load(path: String = "com.etismy") = {
    configTypes.map((t) => byEnv(env, t).getConfig(path).atKey(t))
      .reduce((conf1, conf2) => conf1.withFallback(conf2))
  }

  def byEnv(env: String, name: String): Config =
    catching(classOf[Missing]) either loadF(env + '/' + name) match {
      case Right(config) => env match {
        case `defaultEnv` =>  config
        case _ => config withFallback byEnv(defaultEnv, name)
      }
      case Left(e) => env match {
        case `defaultEnv` =>  throw new DefaultNamespaceMissingException(name, env)
        case _ => byEnv(defaultEnv, name)
      }
    }
}

case class DefaultNamespaceMissingException(name: String, env: String)
    extends Exception("Default namesapce '" + name + "' in environment '" + env +
      "' is missing")

object Sys {

  object Keys {
    val envKey      = "METRICA_ENV"
    val configTypes = List("app", "database")  // TODO: load dynamically from default namespace
  }

  val globalConfig =
    new SysConfig(optionally(System.getenv(Keys.envKey), "development"), Keys.configTypes)
  val config: Config = globalConfig.load("com.metrica")
}
