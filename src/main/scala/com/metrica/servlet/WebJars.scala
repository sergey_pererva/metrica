package com.metrica.servlet

import org.fusesource.scalate.util.IOUtil
import org.scalatra.ScalatraServlet

trait WebJarsSupport {
  self: ScalatraServlet =>

  abstract override def serveStaticResource: Option[Any] =
    self.serveStaticResource orElse serveWebJarsResource

  protected def serveWebJarsResource: Option[Any] = {
    val webJarPrefix = "/lib/"
    if (requestPath startsWith webJarPrefix) {
      val resourcePath = "/META-INF/resources/webjars/" +
        (requestPath stripPrefix webJarPrefix)
      Option(getClass.getResourceAsStream(resourcePath)) match {
        case Some(inputStream) => {
          contentType = servletContext.getMimeType(resourcePath)
          Some(inputStream)
        }
        case None => None
      }
    } else { None }
  }

}
